// ============================
//    Name: index.js
// ============================

import AddMoveScrollUP from './modules/moveScrollUP';
import AddOpenMyBurger from './modules/openMyBurger';
import AddsmoothScrollAnchor from './modules/smoothScrollAnchor';
import AddPhoneInputMask from './modules/phoneInputMask';
import AddValidatorInput from './modules/validatorInput';

const start = (x) => {
	if (x === true) {
		console.log('DOM:', 'DOMContentLoaded', x);

		new AddMoveScrollUP({
			selector: '.js__moveScrollUP',
			speed: 8
		}).run();

		new AddsmoothScrollAnchor({
			selector: '.js__smooth'
		}).run();

		new AddPhoneInputMask({
			selector: '[data-phone="true"]',
			inputid: '#iform__phone'
		}).run();

		new AddValidatorInput({
			selector: '#iform'
		}).run();

		new AddOpenMyBurger({
			burger: '.js__navHamburger',
			navbar: '.js__navHamburgerOpener'
		}).run();

	} else { console.log('System: ', 'I think shit happens 😥 '); }

};

const addCss = (fileName) => {
	const link = document.createElement('link');

	link.type = 'text/css';
	link.rel = 'stylesheet';
	link.href = fileName;

	document.head.appendChild(link);
};

if (typeof window !== 'undefined' && window && window.addEventListener) {

	if (navigator.userAgent.includes('Firefox')) {
		addCss('css/firefox.css');
	} 

	if (navigator.userAgent.includes('Edge')) {
		addCss('css/edge.css');
	}

	document.addEventListener('DOMContentLoaded', start(true), false);
}
