module.exports = class AddPhoneInputMask {
	constructor(init) {
		this.selector = init.selector;
		this.input = init.inputid;
		this.mytarget = document.querySelector(`${this.input}`);
		this.regexp = /^[+|\d?][-\s.()/0-9]*$/;
	}

	static info() { console.log('MODULE:', this.name, true); }

	main() {
		const mycalc = (event) => {
			const space = ' ';
			const getPatter = event.target.getAttribute('data-pattern');
			const resultPattern = new RegExp(`^${getPatter}$`);
			const mask = this.mytarget.value.replace(/\D/g, '').match(resultPattern);

			if (this.mytarget.value.length === 0) return;
			if (!this.mytarget.value.match(this.regexp)) {
				this.mytarget.value = this.mytarget.value.slice(0, -1);
			} else {
				/**
				 * [value if you see it, don't ask me: "What the hell is this, bro ?!]
				 * @type {[type]}
				 */
				this.mytarget.value = !mask[2] ? `+${mask[1]}${space}` : `+${mask[1]}${space}`
					+ (!mask[3] ? `(${mask[2]}` : `(${mask[2]})${space}`) + mask[3]
					+ (mask[4] ? `-${mask[4]}` : '')
					+ (mask[5] ? `-${mask[5]}` : '');
			}

		};

		const whatCode = (event) => {
			if (event.code === 'Backspace') this.mytarget.value = '';
		};

		this.mytarget.addEventListener('input', mycalc, false);
		this.mytarget.addEventListener('keydown', whatCode, false);
	}

	run() {
		if (this.selector) {
			this.constructor.info();
			this.main();
		}
	}
};
