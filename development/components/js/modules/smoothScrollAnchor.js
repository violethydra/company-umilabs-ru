module.exports = class AddsmoothScrollAnchor {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
	}

	static info() { console.log('MODULE:', this.name, true); }

	main() {
		const clickedID = (event) => {
			event.preventDefault();
			let myHref = event.target;
			
			if (!event.target.hasAttribute('href')) myHref = event.target.parentElement;
			document.querySelector(myHref.getAttribute('href')).scrollIntoView({ behavior: 'smooth' });
		};

		this.selector.addEventListener('click', clickedID, false);
	}

	run() {
		if (this.selector) {
			this.constructor.info();
			this.main();
		}
	}
};
