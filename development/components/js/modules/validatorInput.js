module.exports = class AddValidatorInput {
	constructor(init) {
		this.server = init.server;
		this.selector = init.selector;
		this.submitValid = false;
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('MODULE:', this.name, true); }
	
	main() {
		// const tools = document.querySelector('#iform');
		const toolsInput = () => document.querySelectorAll('.iform__input.warning');
		const button = document.querySelector('#iform button');
		const inputs = document.querySelectorAll('#iform input');
		// const selectList = () => document.querySelectorAll('.dropdown__select li');

		const filterEmpty = (item) => {
			if (item.value.length <= 2) item.classList.add('warning');
		};
 
		const nextSubmit = () => {
			console.log('Отправлено!');
			[...document.querySelectorAll('.iform__input')].forEach((el) => {
				const currentElem = el;
				currentElem.style.backgroundColor = '#0000';
				currentElem.style.color = '#fff3';
				currentElem.style.pointerEvents = 'none';
				currentElem.setAttribute('readonly', true);
			});

			button.style.pointerEvents = 'none';
			button.style.backgroundColor = '#5ccc2c';
			button.style.color = '#fff';
			button.innerText = 'Отправлено!';
			button.setAttribute('readonly', true);
		};

		const clickToMyButton = (event) => {
			this.submitValid = false;
			Array.from(inputs).filter(filterEmpty).map(el => el);
			if (toolsInput().length === 0) this.submitValid = true;
			if (this.submitValid) nextSubmit();
		};

		const changeMyInput = (event) => {
			if (event.target.value.length >= 2) event.target.classList.remove('warning');
		};

		// const delayFunc = x => Array.from(x).map((el) => {
		// 	el.addEventListener('click', event => tools.classList.remove('warning'));
		// 	return false;
		// });

		// setTimeout(() => delayFunc(selectList()), 1000);

		button.addEventListener('click', clickToMyButton, false);
		Array.from(inputs).forEach(el => el.addEventListener('input', changeMyInput, false));
	}
	
	run() {
		if (this.selector) {
			this.constructor.info();
			this.main();
		}
	}
};
