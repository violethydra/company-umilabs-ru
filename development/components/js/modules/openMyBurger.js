module.exports = class AddOpenMyBurger {
	constructor(init) {
		this.selector = document.querySelector(init.burger);
		this.navbar = document.querySelector(init.navbar);
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('MODULE:', this.name, true); }

	toggleBurger() {
		const openBurger = () => {
			this.navbar.querySelector('DIV').classList.toggle('open');
			this.selector.classList.toggle('open');
		};
		this.selector.addEventListener('click', openBurger, false);
	}

	run() {
		if (this.selector) {
			this.constructor.info();
			this.toggleBurger();
		}
	}
};
