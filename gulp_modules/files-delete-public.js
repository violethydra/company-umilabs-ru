/* ==== IMPORT PARAMS ==== */
import del from 'del';
/* ==== ----- ==== */

const root = {
	pub: 'public',
	devTemp: 'development\\tmp',
	devPlug: 'development\\components\\plugins'
};

module.exports = () =>
	() => del([
		`${__dirname}\\..\\${root.pub}`,
		`${__dirname}\\..\\${root.devTemp}`,
		`${__dirname}\\..\\${root.devPlug}`
	], { read: false });


