/* ============ INFO ============ *
 * Move JSON file to public location
 *
 * ============ INFO ============/

/* ==== Sources and directions for files ==== */
const
	inDev = 'development',
	inDevApps = `${inDev}/components`,
	inPub = 'public';
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(

		src(`${inDevApps}/json/**/*.{json,JSON}`, ''),
		_run.newer(`${inPub}/media/fonts`),
		dest(`${inPub}/`)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
