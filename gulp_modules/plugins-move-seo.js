/* ==== IMPORT PARAMS ==== */
import { lastRun } from 'gulp';
/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const	inDev = 'development';
const	inDevApps = `${inDev}/components`;
const	inPub = 'public';
/* ==== ----- ==== */

/* ==== Replace URL or Links ==== */
const __cfg = {
	seo: {
		arr: [
			`${inDevApps}/favicon.{png,ico}`,
			`${inDevApps}/robots.txt`,
			`${inDevApps}/.htaccess`,
		]
	}
};
/* ==== ----- ==== */

let sinceReplace = (x) => `${x}`.replace(/-/gi, ':');

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(

		src(__cfg.seo.arr, { since: lastRun(sinceReplace(nameTask)) }),
		_run.newer(`${inPub}/`),
		dest(`${inPub}/`)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
